<?php

namespace Drupal\autoadress\Constant;

/**
 * Settig-Constants.
 *
 * @package Drupal\autoadress\Constant
 */
class AutoAddressSettingConstants {

  /**
   * Constant for autoaddress settings.
   *
   * @var string
   */
  const AUTOADDRESS_SETTINGS = 'autoaddress.settings';

  /**
   * Constant for the algolia credentials.
   *
   * @var string
   */
  const ALGOLIA_CREDENTIALS = 'algolia_credentials';

}
