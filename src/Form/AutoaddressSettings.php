<?php

namespace Drupal\autoadress\Form;

use Drupal\autoadress\Constant\AutoAddressSettingConstants;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settigs Form.
 *
 * @package Drupal\autoadress\Form
 */
class AutoaddressSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      AutoAddressSettingConstants::AUTOADDRESS_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'autoaddress_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(AutoAddressSettingConstants::AUTOADDRESS_SETTINGS);

    $form['description'] = [
      '#markup' => $this->t('Configuration restrict the countries for the address search and to optionally configure the credential for "Algolia Places".'),
    ];

    $form[AutoAddressSettingConstants::ALGOLIA_CREDENTIALS] = [
      '#type' => 'key_select',
      '#key_filters' => [
        'type' => 'user_password',
      ],
      '#title' => $this->t('App Credentials'),
      '#default_value' => $config->get(AutoAddressSettingConstants::ALGOLIA_CREDENTIALS),
      '#empty_option' => $this->t('- Select -'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('autoaddress.settings')
      ->set(AutoAddressSettingConstants::ALGOLIA_CREDENTIALS, $form_state->getValue(AutoAddressSettingConstants::ALGOLIA_CREDENTIALS))
      ->save();
  }

}
