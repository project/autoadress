<?php

namespace Drupal\autoadress\Element;

use Drupal\address\Element\Address;
use Drupal\autoadress\Constant\AutoAddressSettingConstants;
use Drupal\Core\Render\Element\FormElement;
use Drupal;

/**
 * Class AutocompleteAddress.
 *
 * Provides autocomplete form element for addressfield.
 *
 * @package Drupal\autoadress\Element
 *
 * @FormElement("autoaddress")
 */
class AutocompleteAddress extends Address {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    $parentInfo = parent::getInfo();

    $parentInfo['#attached']['library'][] = 'autoadress/autocomplete';
    $parentInfo['#pre_render'][] = [$class, 'placeAlgoliaClass'];
    return $parentInfo;
  }

  /**
   * Change the render array for the autocompeleted addresses.
   *
   * Adds a class to mark the surrounding div of the address.
   * Adds specific settings via drupalSettings for JS.
   *
   * @param array $element
   *   The render array of the address field.
   *
   * @return array
   *   The slightly changed render array for the autoaddress field.
   */
  public static function placeAlgoliaClass(array $element) {
    $element['#attributes']['class'][] = 'autocomplete-address';

    $autoaddressConfig = Drupal::config(AutoAddressSettingConstants::AUTOADDRESS_SETTINGS);
    $drupalSettings = [
      'allowed_countries' => $element['#available_countries'],
    ];
    $algoliaCredentialConfig = $autoaddressConfig->get(AutoAddressSettingConstants::ALGOLIA_CREDENTIALS);
    if ($algoliaCredentialConfig) {
      $algoliaCredentials = Drupal::service('key.repository')
        ->getKey($algoliaCredentialConfig)
        ->getKeyValue();
      $algoliaCredentialsArray = json_decode($algoliaCredentials, TRUE);
      $drupalSettings['appId'] = $algoliaCredentialsArray['username'];
      $drupalSettings['apiKey'] = $algoliaCredentialsArray['password'];
    }

    $element['#attached']['drupalSettings']['autoaddress'] = $drupalSettings;
    return $element;
  }

}
