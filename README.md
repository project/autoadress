# INTRODUCTION
This module provides an integration of 
[Algolia Places](https://community.algolia.com/places/) 
for the [addressfield](https://www.drupal.org/project/address) in a form. 
You can easily swap your old 'address' form element with an autocomplete 
variant by changing the type to 'autoaddress'. 
All filter and configurations of the address field still apply.


This is only for developers.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/autoadress

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/autoadress

# REQUIREMENTS
- [Address](https://www.drupal.org/project/address) (required)

# RECOMMENDED MODULES
- [Key](https://www.drupal.org/project/key) 
(optional: to provide appId and apiKey)

# INSTALLATION
Install and enable the module as usual.

# CONFIGURATION
1. Go to Configuration -> Services -> Algolia Settings and configure the 
optional Fields
2. Just change the element type from 'address' to 'autoaddress' in any form

# Example
This is an example for an address field with autocompletion in Germany. 
```
$form['autoaddress_field'] = [
  '#type' => 'autoaddress',
  '#default_value' => [
    'country_code' => 'DE',
  ],
  '#field_overrides' => [
    AddressField::FAMILY_NAME => FieldOverride::OPTIONAL,
    AddressField::ADDRESS_LINE1 => FieldOverride::OPTIONAL,
    AddressField::POSTAL_CODE => FieldOverride::OPTIONAL,
    AddressField::LOCALITY => FieldOverride::OPTIONAL,
    AddressField::GIVEN_NAME => FieldOverride::OPTIONAL,
    AddressField::ADDRESS_LINE2 => FieldOverride::HIDDEN,
    AddressField::ADDITIONAL_NAME => FieldOverride::HIDDEN,
    AddressField::ORGANIZATION => FieldOverride::HIDDEN,
  ],
  '#available_countries' => [
    'DE',
  ],
  '#required' => TRUE,
];
```
