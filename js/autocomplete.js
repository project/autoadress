(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.autoaddress = {
    attach: function (context, settings) {

      $(".autocomplete-address").once('algolia').each(function () {

        let allowed_countries = drupalSettings.autoaddress.allowed_countries;
        let appId = drupalSettings.autoaddress.appId;
        let apiKey = drupalSettings.autoaddress.apiKey;

        let algolia_config = {
          container: $("input[data-drupal-selector*='address-line1']", this).get(0),
          type: ['address'],
          countries: allowed_countries,
          templates: {
            value: function (suggestion) {
              return suggestion.name;
            }
          }
        };
        if (appId && apiKey) {
          algolia_config.apiKey = apiKey;
          algolia_config.appId = appId;
        }
        let autocomplete = places(algolia_config);

        let surroundingDiv = this;
        autocomplete.on('change', function (e) {
          if(e.suggestion.county){
            $("input[data-drupal-selector*='locality']", surroundingDiv).val(e.suggestion.county);
          }else{
            $("input[data-drupal-selector*='locality']", surroundingDiv).val(e.suggestion.administrative);
          }
          $("input[data-drupal-selector*='postal-code']", surroundingDiv).val(e.suggestion.postcode);
          $("input[data-drupal-selector*='country-code']", surroundingDiv).val(e.suggestion.countryCode.toUpperCase());
          $("input[data-drupal-selector*='address-line2']", surroundingDiv).val(e.suggestion.administrative);
        });
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
